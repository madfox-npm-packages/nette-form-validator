# Simple nette form validator

### Proclamation
This package's name may be a little confusing, because for now the only ability of the package is to validate Nette input form element, based upon its rules. The logic behind validating whole forms need to be added manually. 

### Compatibility
The package is not built and uses ES6 features. Consider using transpiler.

### Installation

`npm install @mdfx/nette-form-validator`

### Usage

The package has only one exposed function `validateElement`, which return either error message or null.

    import { validateElement } from "@mdfx/nette-form-validator";
    
    ...
    
    const errorMessage = validateElement(inputElement)
    
    if (errorMessage === null) {
        console.log('No error')
    } else {
        console.log(errorMessage) 
    }




import { validators } from "./validators";
import validationFunctions from "./validationFunctions";

const validateElement = el => {
    const inputData = JSON.parse(el.dataset.netteRules);
    const errorObjects = validateRules(inputData, el)

    let errorMessage = null
    let errorPriority = 0

    errorObjects.forEach(errorObject => {
        if (errorObject.valid === false && errorObject.priority > errorPriority) {
            errorMessage = errorObject.message
        }
    })

    return errorMessage
};

const validateRules = (rules, el) => {
    let ruleResults = []
    rules.forEach(rule => {
        if (rule.rules) {
            const errorObject = validateRule(rule, el)

            if (errorObject.valid) {
                ruleResults = [...ruleResults, ...validateRules(rule.rules, el)]
            }
        } else {
            ruleResults.push(validateRule(rule, el))
        }
    })

    return ruleResults
}

const validateRule = (validatorData, el) =>
{
    for (const key of Object.keys(validationFunctions)) {
        if (validatorData['op'] !== validators[key].netteName) {
            continue
        }

        const valid = validationFunctions[key]({
            el,
            arg: validatorData['arg'],
            control: validatorData['control'],
        })

        return {
            valid: valid,
            priority: validators[key].priority,
            message: validatorData['msg'],
        }
    }

    return {
        valid: true
    }
}

export {
    validateElement
}

export const validators = {
	equal: {
		netteName: ':equal',
		priority: 100,
	},
	required: {
		netteName: ':filled',
		priority: 5,
	},
	email: {
		netteName: ':email',
		priority: 4,
	},
	pattern: {
		netteName: ':pattern',
		priority: 3,
	},
	minLength: {
		netteName: ':minLength',
		priority: 2,
	},
	maxLength: {
		netteName: ':maxLength',
		priority: 1,
	},
	fileSize: {
		netteName: ':fileSize',
		priority: 6,
	}
};

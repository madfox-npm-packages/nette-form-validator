import isEmail from "validator/lib/isEmail";
import matches from "validator/lib/matches";

const validationFunctions = {
    equal: ({el, arg, control}) => eqValidationFunctionCallback(el, arg, control),
    required: ({el}) => el.value !== '',
    email: ({el}) => isEmail(el.value),
    pattern: ({el, arg}) => matches(el.value, arg),
    minLength: ({el, arg}) => el.value.length >= arg,
    maxLength: ({el, arg}) => el.value.length <= arg,
    fileSize: ({el, arg}) => fileSizeValidationFunctionCallback(el, arg),
}

const eqValidationFunctionCallback = (el, arg, control) => {
    const isControlValueTested = !!arg.control

    if (isControlValueTested) {
        control = arg.control
    }

    const controlElement = document.querySelector(`[name="${control}"]`)

    if ( ! controlElement) {
        return false
    }

    if (isControlValueTested) {
        return controlElement.value === el.value
    } else {
        return controlElement.value === arg || controlElement.checked === arg
    }
}

const fileSizeValidationFunctionCallback = (el, arg) => {
    const files = [...el.files]
    let size = 0

    files.forEach((file) => {
        size += file.size
    })

    return size < arg
}

export default validationFunctions
